#include <stdio.h>

int main (){
    int i, j, k, aux; // variables
for(i=1; i<=7; i++){ // i incrementa
for(j=1; j<=i; j++){ // j aumenta hasta el valor de i
printf("%d ", j); // muestro por pantalla j
}
aux = 0;
for(k=i+1; k<=8; k++){ // k incrementa hasta 8
aux = aux + 1; // por cada vez que k aumenta se incrementa aux
printf("%d ", aux); // muestro aux por pantalla
}
printf("\n"); // salto de linea luego de recorrer cada for 
}

for(i=1; i<=7; i++){
for(j=1; j<=i; j++){ // pruebo por separado cada parte del codigo
printf("%d ", j);
}
printf("\n");
}

for(i=1; i<=7; i++){
   aux = 0;
for(k=i+1; k<=8; k++){ // pruebo por separado cada parte del codigo
aux = aux + 1;
printf("%d ", aux);
}
printf("\n");
}

}